#!/bin/zsh

# install rust
curl --proto '=https' --tlsv1.2 -sSf "https://sh.rustup.rs" | sh

# install homebrew
#/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
#sudo cp /home/joao/scripts/files/brew.sh /etc/profile.d/brew.sh

# install brew apps
#brew install neovim just

# nerdfonnts
# mkdir -p ~/.fonts
# cd ~/.fonts &&
#curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf
curl -fLo "Fire Code Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/FiraMono/Regular/FiraMonoNerdFont-Regular.otf

#curl -fLo "Fire Code Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode/Regular/complete/Fira%20Code%20Regular%20Nerd%20Font%20Complete.ttf
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/DroidSansMono.zip
# flatpak
flatpak install  com.calibre_ebook.calibre 
#org.kde.KStyle.Kvantum

# -a check if sheldon is instaled
if [[ ! -e ~/.local/bin/sheldon ]] 
then

	curl --proto '=https' -fLsS https://rossmacarthur.github.io/install/crate.sh \
    | bash -s -- --repo rossmacarthur/sheldon --to ~/.local/bin

fi

# -a check if starship is instaled
if [[ ! -e ~/.local/bin/starship ]] 
then

	wget https://starship.rs/install.sh
	sh install.sh -b ~/.local/bin
	rm install.sh

fi
