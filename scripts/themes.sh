#!/bin/bash

#  theme
wget "https://github.com/dracula/gtk/archive/master.zip"
unzip ./master.zip -d ~/.local/share/themes/
rename gtk-master dracula ~/.local/share/themes/gtk-master
rm ./master.zip

wget "https://github.com/dracula/gtk/files/5214870/Dracula.zip"
unzip ./Dracula.zip -d ~/.local/share/icons/
rm ./Dracula.zip

#flatpak theme
flatpak install flathub org.kde.KStyle.Kvantum

