. "$HOME/.cargo/env"
export EDITOR=nvim

export NIX_BUILD_SHELL=zsh

if [ -d "/home/linuxbrew/.linuxbrew" ]; then
    fpath=(/home/linuxbrew/.linuxbrew/share/site-functions $fpath)
    export PKG_CONFIG_PATH=PKG_CONFIG_PATH:/home/linuxbrew/.linuxbrew/lib/pkgconfig/
	eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
fi
if [ -d "$HOME/.nix-defexpr/" ]; then
    export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}

    # $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh
fi


if [ -e /home/joao/.nix-profile/etc/profile.d/nix.sh ]; then . /home/joao/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
