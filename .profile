# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# kvantum
#export QT_STYLE_OVERRIDE=kvantum
#export GTK_THEME=dracula

# set PATH so it includes user's miniconda if it exists
if [ -d "$HOME/miniconda3" ] ; then
    PATH="$HOME/miniconda3:$PATH"
fi


# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
# include android studio
if [ -d "$HOME/Android/" ] ; then
    PATH="$JAVA_HOME//bin:$ANDROID_HOME/cmdline-tools/latest/bin:$PATH"
    export JAVA_HOME="/usr/local/jbr/"
    export ANDROID_HOME="$HOME/Android/Sdk"
    export ANDROID_NDK_HOME="$ANDROID_HOME/ndk/26.1.10909125"
fi

# flutter
if [ -d "$HOME/code/Android/flutter/flutter/bin/" ] ; then
    PATH="$HOME/code/Android/flutter/flutter/bin/:$PATH"
fi

 if [ -d "$HOME/.nix-profile/" ] ; then
 	. $HOME/.nix-profile/etc/profile.d/nix.sh
 	. $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh
 	. $HOME/.nix-profile/etc/profile.d/nix-daemon.sh
     # export XDG_DATA_DIRS=$HOME/.nix-profile/share:$XDG_DATA_DIRS
 fi

. "$HOME/.cargo/env"

if [ -d "$HOME/go/bin/" ] ; then
    PATH="$HOME/go/bin/:$PATH"
fi

