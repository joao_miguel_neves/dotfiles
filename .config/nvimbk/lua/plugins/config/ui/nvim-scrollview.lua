-- https://github.com/dstein64/nvim-scrollview
-- local scrollview = require("scrollview")

local M = {}

-- function M.setup() end

function M.config()
    require("scrollview").setup({
        excluded_filetypes = {
            "NvimTree",
            "aerial",
            "undotree",
            "spectre_panel",
            "dbui",
            "lsp-installer",
        },
        -- only show in current window
        current_only = true,
        -- transparency
        winblend = 0,
        -- offset
        base = "right",
        column = 1,
        character = "",
    })
end

return M
