-- https://github.com/goolord/alpha-nvim

local M = {}

function M.config()
	local alpha = require("alpha")
	local dashboard = require("alpha.themes.dashboard")

	dashboard.section.header.val = {

		'',
		'',
		'                                   ██             ',
		'▄▄ ▄▄▄     ▄▄▄▄    ▄▄▄   ▄▄▄▄ ▄▄▄ ▄▄▄  ▄▄ ▄▄ ▄▄   ',
		' ██  ██  ▄█▄▄▄██ ▄█  ▀█▄  ▀█▄  █   ██   ██ ██ ██  ',
		' ██  ██  ██      ██   ██   ▀█▄█    ██   ██ ██ ██  ',
		'▄██▄ ██▄  ▀█▄▄▄▀  ▀█▄▄█▀    ▀█    ▄██▄ ▄██ ██ ██▄ ',
		'                                                  ',
		'                                                  ',
		'',
		'',

	}

	dashboard.section.buttons.val = {
		dashboard.button("SPC f f", "  Find files"),
		dashboard.button("SPC f r", "  Recent files"),
		dashboard.button("SPC f p", "  Projects"),
		dashboard.button("SPC f s", " Sessions"),
		dashboard.button("SPC n c", " Configurations", "<cmd>edit ~/.config/nvim/lua | cd ~/.config/nvim/lua<CR>"),
		dashboard.button("SPC n p", " Sync", "<cmd>PackerSync<CR>")
	}
	alpha.setup(dashboard.config)

end

return M
