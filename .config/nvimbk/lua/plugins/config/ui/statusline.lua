-- https://github.com/nvim-lualine/lualine.nvim
local colors = require('dracula').colors()
local icons = require("core.utils").get_icons("diagnostic", true)
-- local lualine = require("lualine")

local M = {}

-- function M.setup() end
function M.config()
	local conditions = {
		buffer_not_empty = function()
			return vim.fn.empty(vim.fn.expand '%:t') ~= 1
		end,
		hide_in_width = function()
			return vim.fn.winwidth(0) > 80
		end,
		check_git_workspace = function()
			local filepath = vim.fn.expand '%:p:h'
			local gitdir = vim.fn.finddir('.git', filepath .. ';')
			return gitdir and #gitdir > 0 and #gitdir < #filepath
		end,
	}

	-- Config
	local config = {
		options = {
			-- Disable sections and component separators
			section_separators = { left = "", right = "" },
			component_separators = { left = "", right = "" },
			theme = 'dracula-nvim'
		},

		extensions = {
			"toggleterm",
			"aerial",
			"nvim-tree",
			"fugitive",
		},

		sections = {
			-- these are to remove the defaults
			lualine_a = {},
			lualine_b = {},
			lualine_y = {},
			lualine_z = {},
			-- These will be filled later
			lualine_c = {},
			lualine_x = {},
		},
		-- inactive_sections = {
		--   -- these are to remove the defaults
		--   lualine_a = {},
		--   lualine_v = {},
		--   lualine_y = {},
		--   lualine_z = {},
		--   lualine_c = {},
		--   lualine_x = {},
		-- },
	}

	-- Inserts a component in lualine_c at left section
	local function ins_left(component)
		table.insert(config.sections.lualine_c, component)
	end

	-- Inserts a component in lualine_x ot right section
	local function ins_right(component)
		table.insert(config.sections.lualine_x, component)
	end

	local mode_color = {
		n = colors.red,
		i = colors.green,
		v = colors.bright_blue,
		[''] = colors.bright_blue,
		V = colors.bright_blue,
		c = colors.bright_magenta,
		no = colors.red,
		s = colors.orange,
		S = colors.orange,
		[''] = colors.orange,
		ic = colors.yellow,
		R = colors.pink,
		Rv = colors.pink,
		cv = colors.red,
		ce = colors.red,
		r = colors.cyan,
		rm = colors.bright_cyan,
		['r?'] = colors.bright_cyan,
		['!'] = colors.red,
		t = colors.red,
	}

	ins_left {
		function()
			vim.api.nvim_command('hi! LualineMode guifg=' .. mode_color[vim.fn.mode()] .. ' guibg=' .. colors.bg)
			return '▊'
		end,
		color = 'LualineMode',
		padding = { left = 0, right = 1 }, -- We don't need space before this
	}

	ins_left {
		-- mode component
		function()
			-- auto change color according to neovims mode
			vim.api.nvim_command('hi! LualineMode guifg=' .. mode_color[vim.fn.mode()] .. ' guibg=' .. colors.bg)
			return ' '
		end,
		color = 'LualineMode',
		padding = { right = 1 },
	}

	ins_left {
		-- filesize component
		'filesize',
		cond = conditions.buffer_not_empty,
	}

	ins_left {
		'filename',
		cond = conditions.buffer_not_empty,
		color = { fg = colors.bright_magenta, gui = 'bold' },
	}

	ins_left { 'location' }

	ins_left { 'progress', color = { fg = colors.fg, gui = 'bold' } }

	ins_left {
		'diagnostics',
		sources = { 'nvim_diagnostic' },
		symbols = { error = icons.Error, warn = icons.Warn, hint = icons.Hint, info = icons.Info },
		diagnostics_color = {
			color_error = { fg = colors.red },
			color_warn = { fg = colors.yellow },
			color_info = { fg = colors.bright_cyan },
		},
	}

	ins_right {
		-- Lsp server name .
		function()
			local msg = 'No Active Lsp'
			local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
			local clients = vim.lsp.get_active_clients()
			if next(clients) == nil then
				return msg
			end
			for _, client in ipairs(clients) do
				local filetypes = client.config.filetypes
				if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
					return client.name
				end
			end
			return msg
		end,
		icon = ' LSP:',
		color = { fg = colors.bright_cyan, gui = 'bold' },
	}

	-- Add components to right sections

	--ins_right {
	--  'fileformat',
	--  fmt = string.upper,
	--  icons_enabled = false, -- I think icons are cool but Eviline doesn't have them. sigh
	--color = { fg = colors.bright_blue, gui = 'bold' },
	--}

	ins_right {
		'branch',
		icon = '',
		color = { fg = colors.pink, gui = 'bold' },
	}

	ins_right {
		'diff',
		-- Is it me or the symbol for modified us really weird
		symbols = { added = ' ', modified = '柳 ', removed = ' ' },
		diff_color = {
			added = { fg = colors.green },
			modified = { fg = colors.orange },
			removed = { fg = colors.red },
		},
		cond = conditions.hide_in_width,
	}

	ins_right {
		function()
			vim.api.nvim_command('hi! LualineMode guifg=' .. mode_color[vim.fn.mode()] .. ' guibg=' .. colors.bg)
			return '▊'
		end,
		color = 'LualineMode',
		padding = { left = 1 },
	}

	-- function M.config()
	local lualine = require("lualine")
	lualine.setup(config)
end

return M

