Aux = require("plugins.aux.aux_packer")
Category = "ui"

local plugins = {
	{ "nvim-lualine/lualine.nvim", after = { "nvim-web-devicons" },
		config = Aux.config(Category, "statusline")
	},

	{ "mbbill/undotree", event = { "BufRead", "BufNewFile" },
		setup = Aux.setup(Category, "undotree"),
		config = Aux.config(Category, "undotree")
	},

	{ "kyazdani42/nvim-tree.lua",
		cmd = { "NvimTreeToggle", "NvimTreeFindFile" },
		setup = Aux.setup(Category, "nvim-tree"),
		config = Aux.config(Category, "nvim-tree")
	},

	{ "akinsho/bufferline.nvim",
		events = { "BufWinEnter" },
		setup = Aux.setup(Category, "bufferline"),
		config = Aux.config(Category, "bufferline")
	},

	{ "dstein64/nvim-scrollview",
		event = { "BufRead", "BufNewFile" },
		config = Aux.config(Category, "nvim-scrollview")
	},

	{ "akinsho/toggleterm.nvim",
		module = "toggleterm",
		setup = Aux.setup(Category, "toggleterm"),
		config = Aux.config(Category, "toggleterm")
	},

	{ "Mofiqul/dracula.nvim" },

	{ "goolord/alpha-nvim",
		config = Aux.config(Category, "alpha")
	},

	-- { "glepnir/dashboard-nvim",
	-- 	config = Aux.config(Category, "dashboard")
	-- },

	{ 'stevearc/dressing.nvim' },
}

return plugins
