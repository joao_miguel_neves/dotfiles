-- https://github.com/akinsho/bufferline.nvim

local map_register = require("core.utils").bulk_register
local public = require("core.utils.public")
local icons = public.get_icons("diagnostic", true)
-- local bufferline = require("bufferline")

local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	local bufferline = require("bufferline")
	bufferline.setup({
		options = {
			themable = true,
			always_show_bufferline = true,
			show_close_icon = true,
			-- ordinal
			numbers = "none",
			buffer_close_icon = "",
			modified_icon = "●",
			close_icon = "",
			left_trunc_marker = "",
			right_trunc_marker = "",
			diagnostics = "nvim_lsp",
			max_name_length = 18,
			max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
			truncate_names = true, -- whether or not tab names should be truncated
			tab_size = 20,
			separator_style = "slant",
			indicator = { icon = "▎", style = "icon" },
			---@diagnostic disable-next-line: unused-local
			diagnostics_indicator = function(count, level, diagnostics_dict, context)
				local message
				if diagnostics_dict.error then
					message = string.format("%s%s", icons.Error, diagnostics_dict.error)
				elseif diagnostics_dict.warning then
					message = string.format("%s%s", icons.Warn, diagnostics_dict.warning)
				elseif diagnostics_dict.info then
					message = string.format("%s%s", icons.Info, diagnostics_dict.info)
				elseif diagnostics_dict.hint then
					message = string.format("%s%s", icons.Hint, diagnostics_dict.hint)
				else
					message = ""
				end
				return message
			end,
			-- custom_areas = {
			--     right = function()
			--         return {
			--             { text = "  ", guifg = "#f28fad", guibg = "#1A1826" },
			--         }
			--     end,
			-- },
			offsets = {
				{
					filetype = "NvimTree",
					text = "File Explorer",
					highlight = "Directory",
					text_align = "center",
				},
				{
					filetype = "aerial",
					text = "Outline Explorer",
					highlight = "Directory",
					text_align = "center",
				},
				{
					filetype = "undotree",
					text = "Undo Explorer",
					highlight = "Directory",
					text_align = "center",
				},
				{
					filetype = "spectre_panel",
					text = "Project Blurry Search",
					highlight = "Directory",
					text_align = "center",
				},
				{
					filetype = "toggleterm",
					text = "Integrated Terminal",
					highlight = "Directory",
					text_align = "center",
				},
			},
		},
	})
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<c-q>",
			rhs = ":bd<cr>",
			options = { silent = true },
			description = "Close current buffer",
		},
		{
			mode = { "n" },
			lhs = "<s-a>",
			rhs = "<cmd>BufferLineMovePrev<cr>",
			options = { silent = true },
			description = "Move current buffer to left",
		},
		{
			mode = { "n" },
			lhs = "<s-d>",
			rhs = "<cmd>BufferLineMoveNext<cr>",
			options = { silent = true },
			description = "Move current buffer to right",
		},
		{
			mode = { "n" },
			lhs = "<C-Left>",
			rhs = ":BufferLineCyclePrev<cr>",
			options = { silent = true },
			description = "Move to previous buffer"
		},
		{
			mode = { "n" },
			lhs = "<C-Right>",
			rhs = ":BufferLineCycleNext<cr>",
			options = { silent = true },
			description = "Move to next buffer"
		},
		{
			mode = { "n" },
			lhs = "<leader>bo",
			rhs = "#bd|e#|bd#",
			-- function()
			-- {
			--     vim.cmd("BufferLineCloseLeft"),
			--     vim.cmd("BufferLineCloseRight")
			-- },
			-- end,
			options = { silent = true },
			description = "Close all other buffers",
		},
		-- {
		--     mode = { "n" }, -- não
		--     lhs = "<leader>ba",
		--     rhs =
		--     -- function()
		--         {
		--         vim.cmd("BufferLineCloseLeft"),
		--         vim.cmd("BufferLineCloseRight"),
		--         vim.cmd("BufferDelete")
		--         },
		--     -- end,
		--     options = { silent = true },
		--     description = "Close all buffers",
		-- },
		{
			mode = { "n" },
			lhs = "<leader>bt",
			rhs = "<cmd>BufferLinePick<cr>",
			options = { silent = true },
			description = "Go to buffer *",
		},
		{
			mode = { "n" },
			lhs = "<leader>bn",
			rhs = "<cmd>Alpha<cr>",
			options = { silent = true },
			description = "Create new buffer",
		},
		--        {
		--            mode = { "n" },
		--            lhs = "<leader>bh",
		--            rhs = "<cmd>BufferLineCloseLeft<cr>",
		--            options = { silent = true },
		--            description = "Close all left buffers",
		--        },
		--        {
		--            mode = { "n" },
		--            lhs = "<leader>bl",
		--            rhs = "<cmd>BufferLineCloseRight<cr>",
		--            options = { silent = true },
		--            description = "Close all right buffers",
		--        },
	})
end

return M
