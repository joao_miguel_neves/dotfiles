---@diagnostic disable: unknown-diag-code
-- https://github.com/akinsho/toggleterm.nvim

local utils = require("core.utils")
local public = require("core.utils.public")
local map_register = require("core.utils").bulk_register

-- local toggleterm = require("toggleterm")

local M = {}

M.terminals = {
	vert = nil,
	float = nil,
	lazygit = nil,
	gitui = nil,
}

function M.setup()
	M.register_key()
end

function M.config()
	local toggleterm = require("toggleterm")

	M.terms = require("toggleterm.terminal").Terminal

	toggleterm.setup({
		start_in_insert = false,
		auto_scroll = true,
		autochdir = true,
		shade_terminals = true,
		shading_factor = 1,
		size = function(term)
			if term.direction == "horizontal" then
				return vim.o.lines * 0.25
			elseif term.direction == "vertical" then
				return vim.o.columns * 0.25
			end
		end,
		on_open = function()
			vim.wo.spell = false
		end,
		highlights = {
			Normal = {
				link = "Normal",
			},
			NormalFloat = {
				link = "NormalFloat",
			},
			FloatBorder = {
				link = "FloatBorder",
			},
		},
	})

	M.create_terminal()
	M.wrapper_command()
end

function M.create_terminal()
	-- create terminal
	M.terminals.vert = M.terms:new({
		direction = "vertical",
	})

	M.terminals.float = M.terms:new({
		hidden = true,
		count = 120,
		direction = "float",
		float_opts = {
			border = true and "double" or "none",
		},
		on_open = function(term)
			M.open_callback()
			map_register({
				{
					mode = { "t" },
					lhs = "<esc>",
					rhs = "<c-\\><c-n>",
					options = { silent = true, buffer = term.bufnr },
					discope = "Escape float terminal",
				},
				{
					mode = { "n" },
					lhs = "<esc>",
					rhs = [[<cmd>close<cr>]],
					options = { silent = true, buffer = term.bufnr },
					discope = "Escape float terminal",
				}
			})
		end,
		on_close = M.close_callback,
	})

	M.terminals.lazygit = M.terms:new({
		cmd = "lazygit",
		count = 130,
		hidden = true,
		direction = "float",
		float_opts = {
			border = true,
		},
		on_open = function(term)
			M.open_callback()
			utils.register({
				mode = { "i" },
				lhs = "q",
				rhs = "<cmd>close<cr>",
				options = { silent = true, buffer = term.bufnr },
				discope = "Escape lazygit terminal",
			})
		end,
		on_close = M.close_callback,
	})
	M.terminals.gitui = M.terms:new({
		cmd = "gitui",
		count = 130,
		hidden = true,
		direction = "float",
		float_opts = {
			border = true,
		},
		on_open = function(term)
			M.open_callback()
			utils.register({
				mode = { "i" },
				lhs = "q",
				rhs = "<cmd>close<cr>",
				options = { silent = true, buffer = term.bufnr },
				discope = "Escape lazygit terminal",
			})
		end,
		on_close = M.close_callback,
	})
end

function M.wrapper_command()
	local toggleterm = require("toggleterm")
	-- define new method
	toggleterm.vertical_toggle = function()
		--@diagnostic disable-next-line: missing-parameter
		M.terminals.vert:toggle()
	end

	toggleterm.float_toggle = function()
		--@diagnostic disable-next-line: missing-parameter
		M.terminals.float:toggle()
	end

	toggleterm.git_toggle = function()
		vim.ui.select({ "gitui", "lazygit" },
			{
				prompt = 'Select git ui:',
				format_item = function(item)
					return "=> " .. item
				end,
			},
			function(choice)
				if choice == "gitui" then
					M.terminals.gitui:toggle()
				elseif choice == "lazygit" then
					M.terminals.lazygit:toggle()
				end
			end)
	end

	toggleterm.term_toggle = function()
		-- FIX: https://github.com/akinsho/toggleterm.nvim/issues/97#issuecomment-1160323635
		local count = vim.api.nvim_eval("v:count1")
		public.terminal_offset_run_command(string.format("exe %d.'ToggleTerm'", count))
	end
	toggleterm.toggle_all_term = function()
		public.terminal_offset_run_command("ToggleTermToggleAll")
	end
end

function M.open_callback()
	vim.cmd("startinsert")
	utils.unregister({ "t" }, "<esc>")
end

function M.close_callback()
	utils.register({
		mode = { "t" },
		lhs = "<esc>",
		rhs = "<c-\\><c-n>",
		options = { silent = true },
		discope = "Escape terminal insert mode",
	})
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<leader>tt",
			rhs =
			function()
				require("toggleterm").term_toggle()
			end,
			options = { silent = true },
			description = "Toggle bottom or vertical terminal",
		},
		{
			mode = { "n" },
			lhs = "<leader>tf",
			rhs =
			function()
				require("toggleterm").float_toggle()
			end,

			options = { silent = true },
			description = "Toggle floating terminal",
		},
		{
			mode = { "n" },
			lhs = "<leader>tv",
			rhs =
			function()
				require("toggleterm").vertical_toggle()
			end,
			options = { silent = true },
			description = "Toggle vertical terminal",
		},
		{
			mode = { "n" },
			lhs = "<leader>tg",
			rhs =
			function()
				require("toggleterm").git_toggle() -- testar gitui depois
			end,
			options = { silent = true },
			description = "Toggle git terminal",
		},
		{
			mode = { "n" },
			lhs = "<leader>ta",
			rhs =
			function()
				require("toggleterm").toggle_all_term()
			end,
			options = { silent = true },
			description = "Toggle all terminal",
		},
	})
end

return M
