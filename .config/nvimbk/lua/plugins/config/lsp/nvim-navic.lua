-- https://github.com/SmiteshP/nvim-navic

local icons = require("core.utils").get_icons("lsp_kind", true)

local M = {}

function M.setup()
	local ignore_filetype = {
		"dashboard",
		"dap-repl",
		"markdown",
		"NvimTree",
		"aerial",
		"undotree",
	}

	vim.api.nvim_create_autocmd(
		{ "DirChanged", "CursorMoved", "BufWinEnter", "BufFilePost", "InsertEnter", "BufNewFile" },
		{
			callback = function()
				if not vim.bo.buflisted or vim.tbl_contains(ignore_filetype) then
					vim.o.winbar = ""
					return
				end

				vim.o.winbar = "%{%v:lua.require('nvim-navic').get_location()%}"
			end,
		}
	)
end

function M.config()
	local nvim_navic = require("nvim-navic")

	nvim_navic.setup({
		icons = icons,
		highlight = true,
		separator = " > ",
	})
end

return M
