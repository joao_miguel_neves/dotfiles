-- https://github.com/j-hui/fidget.nvim

local M = {}

-- function M.setup() end

function M.config()
	local fidget = require("fidget")
	fidget.setup({
		window = {
			blend = 0,
		},
	})
end

return M
