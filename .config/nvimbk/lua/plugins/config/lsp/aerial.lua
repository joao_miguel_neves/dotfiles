-- https://github.com/stevearc/aerial.nvim

local map_register = require("core.utils").bulk_register
local icons = require("core.utils").get_icons("lsp_kind", false)


local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	local aerial = require("aerial")

	aerial.setup({
		icons = icons,
		show_guides = true,
		backends = { "lsp", "treesitter", "markdown" },
		update_events = "TextChanged,InsertLeave",
		---@diagnostic disable-next-line: unused-local
		on_attach = function(bufnr)
			M.register_key()
		end,
		lsp = {
			diagnostics_trigger_update = false,
			update_when_errors = true,
			update_delay = 300,
		},
		guides = {
			mid_item = "├─",
			last_item = "└─",
			nested_top = "│ ",
			whitespace = "  ",
		},
		layout = { min_width = 30 },
		filter_kind = {
			"Module",
			"Struct",
			"Interface",
			"Class",
			"Constructor",
			"Enum",
			"Function",
			"Method",
		},
	})
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<C-a>",
			rhs = "<cmd>AerialToggle! right<cr>",
			options = { silent = true },
			description = "Open Outilne Explorer",
		},
		{
			mode = { "n", "i" },
			lhs = "<S-Up>",
			rhs = "<cmd>AerialPrev<cr>",
			options = { silent = true },
			description = "Move item up",
		},
		{
			mode = { "n", "i" },
			lhs = "<S-Down>",
			rhs = "<cmd>AerialNext<cr>",
			options = { silent = true },
			description = "Move item down",
		},
		{
			mode = { "n" },
			lhs = "{",
			rhs = "<cmd>AerialPrevUp<cr>",
			options = { silent = true },
			description = "Move up one level",
		},
		{
			mode = { "n" },
			lhs = "}",
			rhs = "<cmd>AerialNextUp<cr>",
			options = { silent = true },
			description = "Move down one level",
		},
	})
end

return M
