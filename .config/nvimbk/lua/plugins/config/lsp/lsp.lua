local map_register = require("core.utils").bulk_register
local aux_lspconfig = require("plugins.aux.lspconfig")

local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	-- local lspconfig = require("lspconfig")
	local nvim_navic = require("nvim-navic")
	local format = require("lsp-format")
	-- local mason_lspconfig = require("mason-lspconfig")
	local trouble = require("trouble")
	local lsp = require('lsp-zero')

	trouble.setup({
		use_diagnostic_signs = true
	})

	format.setup({})

	lsp.ensure_installed({ "lua_ls", "rust_analyzer", "clangd", "cmake", "pyright", "bashls" })
	-- automatic_installation = true,
	-- })

	lsp.preset('recommended')

	lsp.set_preferences({
		set_lsp_keymaps = false,
		configure_diagnostics = false,
		manage_nvim_cmp = false,
	})

	lsp.on_attach(function(client, bufnr)
		nvim_navic.attach(client, bufnr)
		format.on_attach(client)
		vim.api.nvim_exec_autocmds('User', { pattern = 'LspAttached' })
	end)

	lsp.setup()

	require 'lspconfig'.gdscript.setup {}
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<leader>ca",
			rhs = vim.lsp.buf.code_action,
			options = { silent = true },
			description = "Show code action",
		},
		{
			mode = { "n" },
			lhs = "<leader>cn",
			rhs = vim.lsp.buf.rename,
			options = { silent = true },
			description = "Variable renaming",
		},
		{
			mode = { "n" },
			lhs = "<leader>cf",
			rhs = vim.lsp.buf.format,
			options = { silent = true },
			description = "Format buffer",
		},
		{
			mode = { "n" },
			lhs = "gh",
			rhs = vim.lsp.buf.hover,
			options = { silent = true },
			description = "Show help information",
		},
		{
			mode = { "n" },
			lhs = "gr",
			rhs = "<cmd>TroubleToggle lsp_references<cr>", --function()
			-- require("telescope.builtin").lsp_references()
			-- end,
			options = { silent = true },
			description = "Go to references",
		},
		{
			mode = { "n" },
			lhs = "gi",
			rhs = function()
				require("telescope.builtin").lsp_implementations()
			end,
			options = { silent = true },
			description = "Go to implementations",
		},
		{
			mode = { "n" },
			lhs = "gD",
			rhs = "<cmd>TroubleToggle lsp_type_definitions<cr>", --function()
			-- require("telescope.builtin").lsp_type_definitions()
			-- end,
			options = { silent = true },
			description = "Go to type definitions",
		},
		{
			mode = { "n" },
			lhs = "gd",
			rhs = "<cmd>TroubleToggle lsp_definitions<cr>", --function()
			-- require("telescope.builtin").lsp_definitions()
			-- end,
			options = { silent = true },
			description = "Go to definitions",
		},
		{
			mode = { "n" },
			lhs = "gO",
			rhs = "<cmd>TroubleToggle workspace_diagnostics<cr>", --function()
			-- require("telescope.builtin").diagnostics()
			-- end,
			options = { silent = true },
			description = "Show Workspace Diagnostics",
		},
		{
			mode = { "n" },
			lhs = "go",
			rhs = "<cmd>TroubleToggle document_diagnostics<cr>", --aux_lspconfig.diagnostic_open_float,
			options = { silent = true },
			description = "Show Current Diagnostics",
		},
		{
			mode = { "n" },
			lhs = "[g",
			rhs = aux_lspconfig.goto_prev_diagnostic,
			options = { silent = true },
			description = "Jump to prev diagnostic",
		},
		{
			mode = { "n" },
			lhs = "]g",
			rhs = aux_lspconfig.goto_next_diagnostic,
			options = { silent = true },
			description = "Jump to next diagnostic",
		},
		{
			mode = { "i" },
			lhs = "<c-j>",
			rhs = aux_lspconfig.sigature_help,
			options = { silent = true },
			description = "Toggle signature help",
		},
		{
			mode = { "n" },
			lhs = "gq",
			rhs = "<cmd>TroubleToggle quickfix<cr>",
			options = { silent = true },
			description = "Show quickfix list",
		},
		{
			mode = { "n" },
			lhs = "gl",
			rhs = "<cmd>TroubleToggle loclist<cr>",
			options = { silent = true },
			description = "items from the window's location list",
		},
		{
			mode = { "n" },
			lhs = "gt",
			rhs = "<cmd>Trouble<cr>",
			options = { silent = true },
			description = "Focus on Trouble",
		},
		-- {
		--     mode = { "i", "n" },
		--     lhs = "<c-b>",
		--     rhs = aux_lspconfig.scroll_to_up,
		--     options = { silent = true },
		--     description = "Scroll up floating window",
		-- },
		-- {
		--     mode = { "i", "n" },
		--     lhs = "<c-f>",
		--     rhs = aux_lspconfig.scroll_to_down,
		--     options = { silent = true },
		--     description = "Scroll down floating window",
		-- },
		-- {
		--     mode = { "i", "n" },
		--     lhs = "<c-]>",
		--     rhs = aux_lspconfig.focus_float_window(),
		--     options = { silent = true },
		--     description = "Focus floating window",
		-- },
	})
end

return M
