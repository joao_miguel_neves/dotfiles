Aux = require("plugins.aux.aux_packer")

Category = "lsp"
local plugins = {
	{ "williamboman/mason-lspconfig.nvim", after = { "mason.nvim" } },

	{ "SmiteshP/nvim-navic", after = { "mason-lspconfig.nvim" },
		setup = Aux.setup(Category, "nvim-navic"),
		config = -- function()
		Aux.config(Category, "nvim-navic")
		-- 	Aux.after(Category, "nvim-navic")
		-- end
	},

	{ "stevearc/aerial.nvim", after = { "nvim-navic" },
		setup = Aux.setup(Category, "aerial"),
		config = Aux.config(Category, "aerial")
	},

	{ "neovim/nvim-lspconfig", after = { "aerial.nvim" },
		-- setup = Aux.setup(Category, "nvim-lspconfig"),
		-- config = Aux.config(Category, "nvim-lspconfig")
	},

	{ "VonHeikemen/lsp-zero.nvim", after = { "nvim-lspconfig", "nvim-cmp" },
		setup = Aux.setup(Category, "lsp"),
		config = Aux.config(Category, "lsp") },


	{ "j-hui/fidget.nvim", after = { "nvim-lspconfig" },
		config = Aux.config(Category, "fidget")
	},

	{ "kosayoda/nvim-lightbulb", after = { "nvim-lspconfig" },
		config = Aux.config(Category, "nvim-lightbulb")
	},

	{ "jose-elias-alvarez/null-ls.nvim", after = { "nvim-lspconfig" },
		config = Aux.config(Category, "null-ls")
	},

	{ "lukas-reineke/lsp-format.nvim" },

	{ "folke/trouble.nvim", module = "trouble"
		, after = { "nvim-lspconfig" },
		-- config = Aux.config(Category, "trouble"),
		-- setup = Aux.setup(Category, "trouble")
	},
}

return plugins
