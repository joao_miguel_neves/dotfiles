local map_register = require("core.utils").bulk_register

local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	local trouble = require("trouble")

	trouble.setup({
		use_diagnostic_signs = true
	})

end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<leader>ca",
			rhs = vim.lsp.buf.code_action,
			options = { silent = true },
			description = "Show code action",
		}
	})
end

return M
