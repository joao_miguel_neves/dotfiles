-- https://github.com/mickael-menu/zk-nvim

local M = {}

function M.config()
	local zk = require("zk")

	zk.setup({
		lsp = {
			-- `config` is passed to `vim.lsp.start_client(config)`
			config = {
				cmd = { "zk", "lsp" },
				name = "zk",
				on_attach = function(client, bufnr)
					require("nvim-navic").attach(client, bufnr)
					require("aerial").on_attach(client, bufnr)
					vim.api.nvim_exec_autocmds('User', { pattern = 'LspAttached' })
				end
				-- on_attach = ...
				-- etc, see `:h vim.lsp.start_client()`
			},

			-- automatically attach buffers in a zk notebook that match the given filetypes
			auto_attach = {
				enabled = true,
				filetypes = { "markdown" },
			},
		}
	})
end

return M
