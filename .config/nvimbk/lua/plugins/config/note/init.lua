Aux = require("plugins.aux.aux_packer")

Category = "note"
local plugins = {
	{ "mickael-menu/zk-nvim",
		config = Aux.config(Category, "zk")
	},

	{ "nvim-neorg/neorg",
		config = Aux.config(Category, "neorg")
	},

	{ "ellisonleao/glow.nvim", ft = "norg",
		run = ":Neorg sync-parsers",
		config = function()
			require("glow").setup({})
		end
	}
}
return plugins
