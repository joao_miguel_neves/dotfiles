-- https://github.com/nvim-neorg/neorg

local M = {}

function M.config()
	local neorg = require("neorg")

	neorg.setup({
		load = {
			["core.defaults"] = {},
			["core.norg.dirman"] = {
				config = {
					workspaces = {
						home = "~/Documentos/nvim",
					},
					index = "index.norg",
				}
			},
			["core.integrations.treesitter"] = {},
			["core.norg.completion"] = {
				config = { -- Note that this table is optional and doesn't need to be provided
					engine = "nvim-cmp"
				}
			},
			["core.norg.concealer"] = {
				config = {}
			}
		}
	})
end

return M
