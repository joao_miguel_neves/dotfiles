Aux = require("plugins.aux.aux_packer")

Category = "lsp"
local plugins = {
	{ "ellisonleao/glow.nvim",
		cmd = { "Glow" },
		comfig = function()
			require("glow").setup({})
		end },

	{ "IndianBoy42/tree-sitter-just",
		ft = "just",
		config = function()
			require('tree-sitter-just').setup("")
		end
	},

	{ "peterhoeg/vim-qml" },

}

return plugins
