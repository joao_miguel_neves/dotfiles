-- https://github.com/lukas-reineke/indent-blankline.nvim



local M = {}

-- function M.setup()
--     vim.g.indent_blankline_filetype_exclude = {
--         "NvimTree",
--         "aerial",
--         "undotree",
--         "spectre_panel",
--         "dbui",
--         "toggleterm",
--         "notify",
--         "startuptime",
--         "packer",
--         "lsp-installer",
--         "help",
--         "terminal",
--         "lspinfo",
--         "TelescopePrompt",
--         "TelescopeResults",
--         "",
--     }
-- end

function M.config()
	local indent_blankline = require("indent_blankline")

	indent_blankline.setup({
		filetype_exclude = {
			"NvimTree",
			"aerial",
			"undotree",
			"spectre_panel",
			"dbui",
			"toggleterm",
			"notify",
			"startuptime",
			"packer",
			"lsp-installer",
			"help",
			"terminal",
			"lspinfo",
			"TelescopePrompt",
			"TelescopeResults",
			"dashboard",
		},
		show_current_context_start = false,
		show_current_context = true,
		show_end_of_line = true,
	})
end

return M
