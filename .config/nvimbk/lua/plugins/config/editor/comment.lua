-- https://github.com/numToStr/Comment.nvim
-- https://github.com/JoosepAlviste/nvim-ts-context-commentstring

local M = {}

function M.config()
	local comment = require("Comment")
	local comment_utils = require("Comment.utils")
	local ts_context_commentstring_utils = require("ts_context_commentstring.utils")
	local ts_context_commentstring_internal = require("ts_context_commentstring.internal")
	local valid_filtype = "typescriptreact"

	comment.setup({
		sticky = false,
		opleader = {
			line = "gcc",
			block = "gcb",
		},
		toggler = {
			line = "gc",
			block = "gb",
		},
		extra = {
			above = "gck",
			below = "gcj",
			eol = "gca",
		},
		-- pre_hook = function(ctx)
		--     -- FIX: Lua does not distinguish between line and block comments
		--     if vim.tbl_contains(valid_filtype, vim.bo.filetype) then
		--         -- Determine whether to use linewise or blockwise commentstring
		--         local type = ctx.ctype == comment_utils.ctype.linewise and "__default" or "__multiline"

		--         -- Determine the location where to calculate commentstring from
		--         local location = nil
		--         if ctx.cTSConstructortype == comment_utils.ctype.blockwise then
		--             location = ts_context_commentstring_utils.get_cursor_location()
		--         elseif ctx.cmotion == comment_utils.cmotion.v or ctx.cmotion == M.comment_utils.cmotion.V then
		--             location = ts_context_commentstring_utils.get_visual_start_location()
		--         end

		--         return ts_context_commentstring_internal.calculate_commentstring({
		--             key = type,
		--             location = location,
		--         })
		--     end
		-- end,
	})
end

return M
