Aux = require("plugins.aux.aux_packer")
-- TODO: testar o https://github.com/Dax89/ide.nvim

Category = "editor"
local plugins = {
	{ "nvim-telescope/telescope.nvim",
		module = { "telescope" },
		setup = Aux.setup(Category, "telescope"),
		config = Aux.config(Category, "telescope") },

	{ "nvim-telescope/telescope-project.nvim", module = { "telescope._extensions.project" } },

	-- { "nvim-telescope/telescope-fzf-native.nvim", run = "make", module = { "telescope._extensions.fzf" } },

	{ "nvim-telescope/telescope-file-browser.nvim", module = { "telescope._extensions.file_browser" } },

	{ "nvim-telescope/telescope-packer.nvim", module = { "telescope._extensions.packer" } },

	{ "lukas-reineke/indent-blankline.nvim", after = { "nvim-treesitter" },
		config = Aux.config(Category, "indent-blankline") },

	{ "JoosepAlviste/nvim-ts-context-commentstring", after = { "nvim-treesitter" } },

	{ "numToStr/Comment.nvim", keys = { "gb", "gc" },
		config = Aux.config(Category, "comment") },

	{ "mg979/vim-visual-multi",
		fn = { "vm#commands#add_cursor_up", "vm#commands#add_cursor_down" },
		keys = { "<c-n>" },
		setup = Aux.setup(Category, "vim-visual-multi"),
		config = Aux.config(Category, "vim-visual-multi") },

	{ 'tpope/vim-fugitive', cmd = { "G", "Git" } },
}

return plugins
