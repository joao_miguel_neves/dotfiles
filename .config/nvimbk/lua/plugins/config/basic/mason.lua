local icons = require("core.utils").get_icons("server", true)

local M = {}

function M.config()
	local mason = require("mason")

	mason.setup({
		ui = {
			icons = {
				package_installed = icons.installed,
				package_pending = icons.pending,
				package_uninstalled = icons.uninstalled,
			},
		},
	})
end

return M
