-- https://github.com/nvim-treesitter/nvim-treesitter

-- local options = require("core.options")
local nvim_treesitter_configs = require("nvim-treesitter.configs")
-- local nvim_treesitter_parsers = require("nvim-treesitter.parsers")

local M = {}

function M.config()
	nvim_treesitter_configs.setup({
		ensure_installed = { "c", "lua", "rust", "cpp", "python", "gdscript" },
		sync_install = true,
		auto_install = true,
		highlight = {
			enable = true,
			additional_vim_regex_highlighting = false,
		},
		indent = {
			enable = true,
		},
		-- incremental selection
		incremental_selection = {
			enable = false,
			keymaps = {
				init_selection = "<cr>",
				node_incremental = "<cr>",
				node_decremental = "<bs>",
				scope_incremental = "<tab>",
			},
		},
		-- nvim-ts-rainbow
		rainbow = {
			enable = true,
			extended_mode = true,
			-- Do not enable for files with more than 1000 lines, int
			max_file_lines = 1000,
		},
		-- nvim-ts-context-commentstring
		context_commentstring = {
			enable = true,
			enable_autocmd = true,
		},
	})
	-- nvim_treesitter_parsers.get_parser_configs().just = {
	-- 	install_info = {
	-- 		url = "https://github.com/IndianBoy42/tree-sitter-just", -- local path or git repo
	-- 		files = { "src/parser.c", "src/scanner.cc" },
	-- 		branch = "main",
	-- 	},
	-- 	maintainers = { "@IndianBoy42" },
	-- }

end

return M
