Aux = require("plugins.aux.aux_packer")
Category = "basic"

local plugins = {
	{ "wbthomason/packer.nvim" },
	{ "rcarriga/nvim-notify",
		setup = Aux.setup(Category, "nvim-notify"),
		config = Aux.config(Category, "nvim-notify")
	},
	{ "tpope/vim-repeat", fn = "repate#set" },
	{ "nvim-lua/plenary.nvim", module = "plenary", noconfig = true },
	{ "williamboman/mason.nvim", after = { "nvim-notify" },
		config = Aux.config(Category, "mason")
	},
	{ "nvim-treesitter/nvim-treesitter",
		module = "nvim-treesitter",
		run = { ":TSUpdate" },
		config = Aux.config(Category, "nvim-treesitter")
	},
	{ "kyazdani42/nvim-web-devicons", module = "nvim-web-devicons" },
	{ "nvim-lua/popup.nvim" },
	{ "antoinemadec/FixCursorHold.nvim" },
}

return plugins
