-- https://github.com/rcarriga/nvim-notify

local map_register = require("core.utils").bulk_register
local icons = require("core.utils").get_icons("diagnostic", true)
-- local notify = require("notify")

local M = {}

function M.setup()
    M.register_key()
end

function M.config()
    local notify = require("notify")
    notify.setup({
            stages = "fade_in_slide_out",
            timeout = 3000,
            fps = 120,
            icons = {
                ERROR = icons.Error,
                WARN = icons.Warn,
                INFO = icons.Hint,
            },
    })
    vim.notify = notify

end

function M.register_key()
    map_register({
        {
            mode = { "n" },
            lhs = "<leader>nf",
            rhs = ":Notifications<cr>",
            options = { silent = true },
            description = "Find notices history",
        },
    })
end

return M
