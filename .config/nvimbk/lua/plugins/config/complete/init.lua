Aux = require("plugins.aux.aux_packer")

Category = "complete"
local plugins = {
	{ "rafamadriz/friendly-snippets", event = { "InsertEnter", "CmdlineEnter" } },

	{ "L3MON4D3/LuaSnip", after = { "friendly-snippets" },
		-- setup = Aux.setup(Category, "luasnip"),
		config = Aux.config(Category, "luasnip")
	},

	{ "hrsh7th/nvim-cmp", after = { "LuaSnip" },
		config = Aux.config(Category, "nvim-cmp")
	},

	{ "saadparwaiz1/cmp_luasnip", after = { "nvim-cmp" } },

	{ "hrsh7th/cmp-nvim-lsp", after = { "nvim-cmp" } },

	{ "hrsh7th/cmp-buffer", after = { "nvim-cmp" } },

	{ "hrsh7th/cmp-path", after = { "nvim-cmp" } },

	{ "hrsh7th/cmp-cmdline", after = { "nvim-cmp" } },

	{ "hrsh7th/cmp-nvim-lua", after = { "nvim-cmp" } },

	{ "onsails/lspkind.nvim", module = "lspkind", after = { "nvim-cmp" } },
}
return plugins
