-- https://github.com/hrsh7th/nvim-cmp
-- https://github.com/L3MON4D3/LuaSnip
-- https://github.com/saadparwaiz1/cmp_luasnip
-- https://github.com/hrsh7th/cmp-path
-- https://github.com/hrsh7th/cmp-buffer
-- https://github.com/hrsh7th/cmp-cmdline
-- https://github.com/rafamadriz/friendly-snippets
-- https://github.com/tzachar/cmp-tabnine
-- https://github.com/kristijanhusak/vim-dadbod-completion

--ARN: manually install tabnine when there is an error in tabnine
--    TabNine is not executable
-- requires curl and unzip
--    ~/.local/share/nvim/site/pack/packer/opt/cmp-tabnine/install.sh

-- local aux_cmp = require("utils.aux.nvim-cmp")
local icons = require("core.utils").get_icons("lsp_kind", false)

local M = {
	-- complete the floating window settings of the menu
	complete_window_settings = {
		fixed = true,
		min_width = 20,
		max_width = 20,
	},

	-- whether to allow the following completion sources to have the same keywords as other completion sources
	duplicate_keywords = {
		-- allow duplicate keywords
		["luasnip"] = 1,
		["nvim_lsp"] = 1,
		-- do not allow duplicate keywords
		["buffer"] = 0,
		["path"] = 0,
		["cmdline"] = 0,
		["cmp_tabnine"] = 0,
		["vim-dadbod-completion"] = 0,
	},
}


local has_words_before = function()
	local line, col = table.unpack(vim.api.nvim_win_get_cursor(0))
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

function M.config()
	local cmp_status_ok, cmp = pcall(require, "cmp")
	if not cmp_status_ok then
		return
	end

	local snip_status_ok, luasnip = pcall(require, "luasnip")
	if not snip_status_ok then
		return
	end

	require("luasnip/loaders/from_vscode").lazy_load()

	local check_backspace = function()
		local col = vim.fn.col(".") - 1
		return col == 0 or vim.fn.getline("."):sub(col, col):match("%s")
	end

	local kind_icons = {
		Text = "",
		Method = "",
		Function = "",
		Constructor = "",
		Field = "",
		Variable = "",
		Class = "",
		Interface = "",
		Module = "",
		Property = "",
		Unit = "",
		Value = "",
		Enum = "",
		Keyword = "",
		Snippet = "",
		Color = "",
		File = "",
		Reference = "",
		Folder = "",
		EnumMember = "",
		Constant = "",
		Struct = "",
		Event = "",
		Operator = "",
		TypeParameter = "",
	}

	cmp.setup({
		snippet = {
			expand = function(args)
				luasnip.lsp_expand(args.body) -- For `luasnip` users.
			end,
		},

		mapping = cmp.mapping.preset.insert({
			-- ["<C-Up>"] = cmp.mapping.select_prev_item(),
			-- ["<C-Down>"] = cmp.mapping.select_next_item(),
			["<C-Up>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
			["<C-Down>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
			["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
			["<esc>"] = cmp.mapping({
				i = cmp.mapping.abort(),
				c = cmp.mapping.close(),
			}),
			-- Accept currently selected item. If none selected, `select` first item.
			-- Set `select` to `false` to only confirm explicitly selected items.
			["<CR>"] = cmp.mapping.confirm({ select = true }),
			["<Tab>"] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif luasnip.expandable() then
					luasnip.expand()
				elseif luasnip.expand_or_jumpable() then
					luasnip.expand_or_jump()
				elseif check_backspace() then
					fallback()
				else
					fallback()
				end
			end, {
				"i",
				"s",
			}),
			["<S-Tab>"] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif luasnip.jumpable(-1) then
					luasnip.jump(-1)
				else
					fallback()
				end
			end, {
				"i",
				"s",
			}),
		}),
		-- formatting = {
		-- 	fields = { "kind", "abbr", "menu" },
		-- 	format = function(entry, vim_item)
		-- 		vim_item.kind = kind_icons[vim_item.kind]
		-- 		vim_item.menu = ({
		-- 			nvim_lsp = "",
		-- 			luasnip = "",
		-- 			nvim_lua = "",
		-- 			buffer = "",
		-- 			path = "",
		-- 			emoji = "",
		-- 		})[entry.source.name]
		-- 		return vim_item
		-- 	end,
		-- },
		sources = {
			{ name = "nvim_lsp" },
			{ name = "nvim_lua" },
			{ name = "luasnip" },
			{ name = "path" },
			{ name = "buffer" },
			{ name = "neorg" },
		},
		confirm_opts = {
			behavior = cmp.ConfirmBehavior.Replace,
			select = false,
		},
		window = {
			completion = cmp.config.window.bordered(),
			documentation = cmp.config.window.bordered(),
		},

		formatting = {


			format = require("lspkind").cmp_format({
				-- mode = 'symbol_text',
				maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
				ellipsis_char = '...', -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
			})

			-- format = function(entry, vim_item)
			-- 	local kind = vim_item.kind
			-- 	local source = entry.source.name
			--
			-- 	vim_item.kind = string.format("%s %s", icons[kind], kind)
			-- 	vim_item.menu = string.format("<%s>", string.upper(source))
			-- 	vim_item.dup = M.duplicate_keywords[source] or 0
			--
			-- 	-- determine if it is a fixed window size
			-- 	if M.complete_window_settings.fixed and vim.fn.mode() == "i" then
			-- 		local label = vim_item.abbr
			-- 		local min_width = M.complete_window_settings.min_width
			-- 		local max_width = M.complete_window_settings.max_width
			-- 		local truncated_label = vim.fn.strcharpart(label, 0, max_width)
			--
			-- 		if truncated_label ~= label then
			-- 			vim_item.abbr = string.format("%s %s", truncated_label, "…")
			-- 		elseif string.len(label) < min_width then
			-- 			local padding = string.rep(" ", min_width - string.len(label))
			-- 			vim_item.abbr = string.format("%s %s", label, padding)
			-- 		end
			-- 	end
			--
			-- 	return vim_item
			-- end,
		},

		experimental = {
			ghost_text = true,
		},
	})
	-- end
	--
	-- function M.after()
	-- define completion in cmd mode
	cmp.setup.cmdline("/", {
		sources = {
			{ name = "buffer" },
		},
	})

	cmp.setup.cmdline("?", {
		sources = {
			{ name = "buffer" },
		},
	})

	cmp.setup.cmdline(":", {
		sources = cmp.config.sources({
			{ name = "path" },
			{ name = "cmdline" },
		}),
	})
end

return M
