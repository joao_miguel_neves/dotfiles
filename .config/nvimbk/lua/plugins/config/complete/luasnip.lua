-- https://github.com/hrsh7th/vim-vsnip

-- local api = require("utils.api")
-- local options = require("core.options")
-- local map_register = require("core.utils").bulk_register

local M = {}


-- function M.setup()
-- M.register_key()
-- end

function M.config()
	require("luasnip.loaders.from_vscode").lazy_load()
	require 'luasnip'.filetype_extend("dart", { "flutter" })
	-- TODO: poder baixar snippets do vscode
end

-- function M.register_key()
-- 	map_register({
-- {
-- 	mode = { "i", "s" },
-- 	lhs = "<tab>",
-- 	rhs = "vsnip#jumpable(1)? '<Plug>(vsnip-jump-next)':'<tab>'",
-- 	options = { silent = true, expr = true },
-- 	description = "Jump to the next fragment placeholder",
-- },
-- {
-- 	mode = { "i", "s" },
-- 	lhs = "<s-tab>",
-- 	-- TODO: Mapping modification
-- 	rhs = function()
-- 		return vim.fn["vsnip#jumpable"](-1) and "<Plug>(vsnip-jump-prev)" or "<s-tab>"
-- 	end,
-- 	options = { silent = true, expr = true },
-- 	description = "Jump to the prev fragment placeholder",
-- },
-- 	})
-- end

return M
