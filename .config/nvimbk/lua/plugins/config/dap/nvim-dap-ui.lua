-- https://github.com/rcarriga/nvim-dap-ui

local map_register = require("core.utils").bulk_register
local public = require("core.utils.public")

local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	local dapui = require("dapui")
	local dap = require("dap")
	dapui.setup({
		layouts = {
			{
				elements = {
					-- elements can be strings or table with id and size keys.
					"scopes",
					"breakpoints",
					"stacks",
					"watches",
				},
				size = 30,
				position = "right",
			},
			{
				elements = {
					"repl",
					"console",
				},
				size = 10,
				position = "bottom",
			},
		},
	})
	-- end
	--
	-- function M.after()
	-- Automatically start dapui when debugging starts
	dap.listeners.after.event_initialized["dapui_config"] = function()
		---@diagnostic disable-next-line: missing-parameter
		dapui.open()
	end
	-- Automatically close dapui and repl windows when debugging is closed
	dap.listeners.before.event_terminated["dapui_config"] = function()
		---@diagnostic disable-next-line: missing-parameter
		dapui.close()
		dap.repl.close()
	end
	-- Automatically close dapui and repl windows when debugging is closed
	dap.listeners.before.event_exited["dapui_config"] = function()
		---@diagnostic disable-next-line: missing-parameter
		dapui.close()
		dap.repl.close()
	end
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<leader>du",
			rhs = function()
				---@diagnostic disable-next-line: missing-parameter
				require("dapui").toggle()
			end,
			options = { silent = true },
			description = "Toggle debug ui",
		},
		{
			mode = { "n" },
			lhs = "<leader>de",
			rhs = function()
				for _, opts in ipairs(public.get_all_win_buf_ft()) do
					if opts.buf_ft == "dapui_hover" then
						---@diagnostic disable-next-line: missing-parameter
						require("dapui").eval()
						return
					end
				end
				---@diagnostic disable-next-line: missing-parameter
				require("dapui").eval(vim.fn.input("Enter debug expression: "))
			end,
			options = { silent = true },
			description = "Execute debug expressions",
		},
	})
end

return M
