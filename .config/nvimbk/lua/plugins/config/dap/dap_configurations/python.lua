-- python3 -m pip install debugpy

return {
	adapters = {
		python = {
			type = "executable",
			command = table.concat({ vim.fn.stdpath("data"), "mason", "bin/debugpy" }, "/"),
			-- args = { "-m", "debugpy.adapter" },
		},
	},
	configurations = {
		python = {
			{
				type = "python",
				request = "launch",
				name = "Launch file",
				program = "${file}",
				pythonPath = "python3",
			},
			{
				type = "python",
				request = "launch",
				name = "Launch",
				program = function()
					return vim.fn.input('Path to python ', vim.fn.getcwd() .. '/', 'file')
				end,
				pythonPath = "python3",
			},
		},
	},
}
