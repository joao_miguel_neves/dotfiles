--local dap = require('dap')
return {
	adapters = {
		lldb = {
			type = "executable",
			name = "lldb",
			command = table.concat({ vim.fn.stdpath("data"), "mason", "bin/codelldb" }, "/"),
		},
	},
	configurations = {
		cpp = {
			{
				name = 'Launch',
				type = 'lldb',
				request = 'launch',
				program = function()
					return vim.ui.input({ 'Path to executable: ', vim.fn.getcwd() .. '/', 'file' }, function() end)
				end,
				cwd = '${workspaceFolder}',
				stopOnEntry = false,
				args = {},
				runInTerminal = false,
			},
		},
		c = {
			{
				name = 'Launch',
				type = 'lldb',
				request = 'launch',
				program = function()
					return vim.ui.input({ 'Path to executable: ', vim.fn.getcwd() .. '/', 'file' }, function() end)
				end,
				cwd = '${workspaceFolder}',
				stopOnEntry = false,
				args = {},
				runInTerminal = false,
			},
		},
		rust = {
			{
				name = 'Launch',
				type = 'lldb',
				request = 'launch',
				program = function()
					return vim.ui.input({ 'Path to executable: ', vim.fn.getcwd() .. '/target/debug', 'file' }, function() end)
				end,
				cwd = '${workspaceFolder}',
				stopOnEntry = false,
				args = {},
				runInTerminal = false,
			},

		},
	},
}
