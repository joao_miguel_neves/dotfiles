return {
	adapters = {
		nlua = function(callback, config)
			local osv_conf = require("osv").launch({ port = config.port or 8086 })
			callback({ type = 'server', host = config.host or "127.0.0.1", port = config.port or osv_conf.port })
		end

	},
	configurations = {
		lua = {
			{
				type = 'nlua',
				request = 'attach',
				name = "Attach to running Neovim instance",

			},
			-- {
			-- 	type = 'nlua',
			-- 	request = 'attach',
			-- 	name = "Attach to running Neovim instance",
			-- 	host = function()
			-- 		local value = vim.fn.input('Host [127.0.0.1]: ')
			-- 		if value ~= "" then
			-- 			return value
			-- 		end
			-- 		return '127.0.0.1'
			-- 	end,
			-- 	port = function()
			-- 		local val = tonumber(vim.fn.input('Port: '))
			-- 		assert(val, "Please provide a port number")
			-- 		return val
			-- 	end,
			-- },
		},

	},
}
