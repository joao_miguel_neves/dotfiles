-- https://github.com/mfussenegger/nvim-dap

-- local api = require("utils.api")
local map_register = require("core.utils").bulk_register

local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	local dap = require("dap")
	require("mason-tool-installer").setup {
		ensure_installed = { "codelldb", "bash-debug-adapter", "netcoredbg", "debugpy" },
		auto_update = false,
		run_on_start = true,
	}
	local configurations_dir_path = "plugins.config.dap.dap_configurations"

	local require_file_tbl = function()
		local sub_dir = configurations_dir_path:gsub("%.", "/")
		local root_dir = table.concat({ vim.fn.stdpath("config"), "lua", sub_dir }, "/")

		--@diagnostic disable-next-line: param-type-mismatch
		local file_tbl = vim.fn.globpath(root_dir, "*.lua", false, true)

		for i, v in ipairs(file_tbl) do
			file_tbl[i] = string.format("%s/%s", sub_dir, vim.fn.fnamemodify(v, ":t:r"))
		end

		return file_tbl --api.path.get_importable_subfiles(configurations_dir_path)
	end

	for _, require_file in ipairs(require_file_tbl()) do
		local dap_config = require(require_file)
		dap.adapters = vim.tbl_deep_extend("force", dap.adapters, dap_config.adapters)
		dap.configurations = vim.tbl_deep_extend("force", dap.configurations, dap_config.configurations)
	end
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<leader>db",
			rhs = function()
				require("dap").toggle_breakpoint()
			end,
			options = { silent = true },
			description = "Mark or delete breakpoints",
		},
		{
			mode = { "n" },
			lhs = "<leader>dc",
			rhs = function()
				require("dap").clear_breakpoints()
			end,
			options = { silent = true },
			description = "Clear breakpoints in the current buffer",
		},
		{
			mode = { "n" },
			lhs = "<F5>",
			rhs = function()
				require("dap").continue()
			end,
			options = { silent = true },
			description = "Enable debugging or jump to the next breakpoint",
		},
		{
			mode = { "n" },
			lhs = "<F6>",
			rhs = function()
				require("dap").step_into()
			end,
			options = { silent = true },
			description = "Step into",
		},
		{
			mode = { "n" },
			lhs = "<F7>",
			rhs = function()
				---@diagnostic disable-next-line: missing-parameter
				require("dap").step_over()
			end,
			options = { silent = true },
			description = "Step over",
		},
		{
			mode = { "n" },
			lhs = "<F8>",
			rhs = function()
				require("dap").step_out()
			end,
			options = { silent = true },
			description = "Step out",
		},
		{
			mode = { "n" },
			lhs = "<F9>",
			rhs = function()
				require("dap").run_last()
			end,
			options = { silent = true },
			description = "Rerun debug",
		},
		{
			mode = { "n" },
			lhs = "<F10>",
			rhs = function()
				require("dap").terminate()
			end,
			options = { silent = true },
			description = "Close debug",
		},
	})
end

return M
