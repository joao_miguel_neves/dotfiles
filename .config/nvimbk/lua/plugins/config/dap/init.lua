Aux = require("plugins.aux.aux_packer")
Category = "dap"
local plugins = {
	{ "mfussenegger/nvim-dap", module = "dap",
		setup = Aux.setup(Category, "nvim-dap"),
		config = Aux.config(Category, "nvim-dap")
	},

	{ "theHamsta/nvim-dap-virtual-text", after = { "nvim-dap" },
		config = Aux.config(Category, "nvim-dap-virtual-text")
	},

	{ "rcarriga/nvim-dap-ui", after = { "nvim-dap" },
		setup = Aux.setup(Category, "nvim-dap-ui"),
		config = Aux.config(Category, "nvim-dap-ui")
	},

	{ "jbyuki/one-small-step-for-vimkind" }, --, after = { "nvim-dap" } },

	{ "WhoIsSethDaniel/mason-tool-installer.nvim", after = { "mason.nvim" } },
}
return plugins
