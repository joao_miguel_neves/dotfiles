-- https://github.com/theHamsta/nvim-dap-virtual-text

local M = {}

function M.config()
	require("nvim-dap-virtual-text").setup()
end

return M
