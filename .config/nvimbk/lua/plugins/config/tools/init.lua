Aux = require("plugins.aux.aux_packer")

Category = "tools"
local plugins = {
	{ "lewis6991/impatient.nvim" },

	{ "lambdalisue/suda.vim" },

	{ "glacambre/firenvim", run = { function() vim.fn['firenvim#install'](0) end } },

	{ "lewis6991/gitsigns.nvim", event = { "BufRead", "BufNewFile" },
		-- setup =  aux.setup(Category, "gitsigns"),
		config = Aux.config(Category, "gitsigns") },

	{ "folke/which-key.nvim", event = { "BufRead", "BufNewFile" },
		config = Aux.config(Category, "which-key") },

	{ "olimorris/persisted.nvim",
		setup = Aux.setup(Category, "persisted"),
		config = Aux.config(Category, "persisted") },

	{ "windwp/nvim-autopairs",
		config = Aux.config(Category, "nvim-autopairs") },

	{ "RRethy/vim-illuminate",
		config = Aux.config(Category, "vim-illuminate") },

	{ "folke/todo-comments.nvim",
		setup = Aux.setup(Category, "todo-comments"),
		config = Aux.config(Category, "todo-comments") },

	{ "norcalli/nvim-colorizer.lua",
		config = function()
			Aux.config(Category, "nvim-colorizer")
			Aux.after(Category, "nvim-colorizer")
		end },

	{ "jghauser/mkdir.nvim" },

	{
		"ahmedkhalf/project.nvim",
		config = Aux.config(Category, "project")
	},
}
return plugins
