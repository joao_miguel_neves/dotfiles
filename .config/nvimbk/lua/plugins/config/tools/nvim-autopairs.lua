-- https://github.com/windwp/nvim-autopairs

local M = {}

-- function M.setup() end

function M.config()
	local nvim_autopairs = require("nvim-autopairs")
   
	nvim_autopairs.setup({
        map_c_h = true,
        map_c_w = true
    })
end

return M
