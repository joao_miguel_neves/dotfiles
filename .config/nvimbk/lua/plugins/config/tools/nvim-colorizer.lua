-- https://github.com/norcalli/nvim-colorizer.lua

local M = {}

function M.config()
	local colorizer = require("colorizer")

	colorizer.setup()
end

function M.after()
    vim.cmd("ColorizerReloadAllBuffers")
end

return M
