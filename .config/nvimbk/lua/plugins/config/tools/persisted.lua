-- https://github.com/olimorris/persisted.nvim
local map_register = require("core.utils").bulk_register
local options = require("core.options")

local M = {}

function M.setup()
	M.register_key()
end

function M.config()
	local persisted = require("persisted")

	persisted.setup({
		-- save_dir = vim.fn.stdpath("cache") .. "/sessions",
		-- use_git_branch = true,
		-- command = "VimLeavePre",
		branch_separator = "_",
		autosave = true,
		allowed_dirs = {
			options.project_path,
		},
		-- before_save = function()
		-- 	vim.cmd([[:wa]])
		-- end,
		telescope = {
			before_source = function()
				-- Close all open buffers
				vim.api.nvim_input("<ESC>:%bd<CR>")
			end,
			after_source = function(session)
				vim.notify("Loaded session " .. session.name)
			end,
		}

	})
end

function M.register_key()
	map_register({
		{
			mode = { "n" },
			lhs = "<leader>sl",
			rhs = function()
				require("persisted").load()
			end,
			options = { silent = true },
			description = "Load session",
		},
		{
			mode = { "n" },
			lhs = "<leader>ss",
			rhs = function()
				-- vim.cmd("SessionSave<cr>")
				require("persisted").save()
				vim.notify("Session saved success")
			end,
			options = { silent = true },
			description = "Save session",
		},
		{
			mode = { "n" },
			lhs = "<leader>sd",
			rhs = function()
				local ok, _ = pcall(require("persisted").delete, nil)
				if ok then
					vim.notify("Session deleted success")
				else
					vim.notify("Session deleted failure")
				end
			end,
			options = { silent = true },
			description = "Delete session",
		},
	})
end

return M
