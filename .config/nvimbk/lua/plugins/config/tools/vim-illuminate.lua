-- https://github.com/RRethy/vim-illuminate

local M = {}

-- function M.setup() end

function M.config()
	local illuminate = require("illuminate")

	illuminate.configure({
		delay = 100,
		under_cursor = true,
		modes_denylist = { "t" },
		providers = {
			"lsp",
			"regex",
			"treesitter",
		},
		filetypes_denylist = {
			"NvimTree",
			"aerial",
			"undotree",
			"spectre_panel",
			"dbui",
			"toggleterm",
			"notify",
			"startuptime",
			"packer",
			"lsp-installer",
			"help",
			"terminal",
			"lspinfo",
			"TelescopePrompt",
			"TelescopeResults",
			"",
		},
	})
end

return M
