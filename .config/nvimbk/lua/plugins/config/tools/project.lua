local options = require("core.options")

local M = {}

function M.config()

	require("project_nvim").setup({
		patterns = {
			"> " .. options.project_path
		},
		detection_methods = { "pattern", "lsp" },
		-- detection_methods = { "pattern" },
	})

	require('telescope').load_extension('projects')

end

return M
