local aux_packer = {}

function aux_packer.start()
    local fn = vim.fn
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        vim.notify("Download packer ...", "INFO", { title = "Packer" })
        fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

function aux_packer.load_module(plugins)
    for _, plugin in ipairs(plugins) do
        require("packer").use(plugin)
    end
end

function aux_packer.config(category ,pluginconf)
    return string.format([[require("plugins.config.%s.%s").config()]], category, pluginconf)
end

function aux_packer.setup(category, pluginconf)
    return string.format([[require("plugins.config.%s.%s").setup()]], category, pluginconf)
end

function aux_packer.after(category, pluginconf)
    return string.format([[require("plugins.config.%s.%s").setup()]], category, pluginconf)
end

return aux_packer
