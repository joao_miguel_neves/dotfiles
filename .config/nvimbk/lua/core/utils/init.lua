local M = {}

function M.register(key_map)
    key_map.options.desc = key_map.description
    vim.keymap.set(key_map.mode, key_map.lhs, key_map.rhs, key_map.options)
end

function M.unregister(mode, lhs, opts)
    vim.keymap.del(mode, lhs, opts)
end

function M.bulk_register(group_keymap)
    for _, key_map in pairs(group_keymap) do
        M.register(key_map)
    end
end

function M.get_icons(name, fill)
    local icons = require("core.utils.icons")

    if not fill then
        return icons[name]
    end

    local fill_icons = {}

    for icon_name, icon_value in pairs(icons[name]) do
        fill_icons[icon_name] = string.format("%s ", icon_value)
    end

    return fill_icons
end

return M