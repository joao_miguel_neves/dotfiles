local g = vim.g

g.mapleader = " "

local opt          = vim.o
opt.incsearch      = true
opt.tabstop        = 4
opt.softtabstop    = 4
opt.ruler          = false
opt.showmode       = false
opt.swapfile       = false
opt.signcolumn     = "auto:5"
opt.completeopt    = "menu,menuone"
opt.pumheight      = 10
opt.updatetime     = 500
opt.timeoutlen     = 500
opt.termguicolors  = true
opt.cursorline     = true
opt.numberwidth    = 2
opt.number         = true
opt.scrolloff      = 21
opt.mouse          = "a"
opt.ignorecase     = true
opt.smartcase      = true
opt.filetype       = "plugin"
opt.foldmethod     = "indent"
opt.foldlevel      = 100
opt.foldcolumn     = "0"
opt.clipboard      = "unnamedplus"
opt.smartindent    = true
opt.laststatus     = 3
opt.linebreak      = true
opt.fillchars      = "eob: "
-- opt.fileencodings = "ucs-bomutf-8,gbk,big5,gb18030,latin1"
opt.ttyfast        = true
opt.showmatch      = true
opt.shiftwidth     = 4
opt.autoindent     = true
opt.sessionoptions = "buffers,curdir,folds,winpos,winsize"

vim.cmd([[colorscheme dracula]])

local options = {}

options.project_path = "~/code/Projects"

-- options.snippet_directory = ""

return options
