vim.cmd([[
if exists("g:gnvim")
    gnvim#enable_ext_cmdline
    gnvim#enable_ext_popupmeu
endif
]])

vim.g.gui_font_default_size = 12
vim.g.gui_font_size = vim.g.gui_font_default_size

vim.g.gui_font_face = "FiraCode Nerd Font" -- Regular"

RefreshGuiFont = function()
	vim.opt.guifont = string.format("%s:h%s", vim.g.gui_font_face, vim.g.gui_font_size)
end

ResizeGuiFont = function(delta)
	vim.g.gui_font_size = vim.g.gui_font_size + delta
	RefreshGuiFont()
end

ResetGuiFont = function()
	vim.g.gui_font_size = vim.g.gui_font_default_size
	RefreshGuiFont()
end

-- Call function on startup to set default value
ResetGuiFont()

-- -- Keymaps
--
-- -- local opts = { noremap = true, silent = true }
require("core.utils").bulk_register({
	{
		mode = { "n" },
		lhs = "<C-=>",
		rhs = function()
			ResizeGuiFont(1)
		end,
		options = { silent = true },
		description = "increase gui font size"
	},
	{
		mode = { "n" },
		lhs = "<C-->",
		rhs = function()
			ResizeGuiFont(-1)
		end,
		options = { silent = true },
		description = "increase gui font size"
	},
})
