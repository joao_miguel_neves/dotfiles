local register = require("core.utils").bulk_register

register({
	{
		mode = { "t" },
		lhs = "<esc>",
		rhs = "<c-\\><c-n>",
		options = { silent = true },
		description = "Escape terminal insert mode",
	},
	{
		mode = { "n" },
		lhs = "<esc>",
		rhs = ":noh<cr>",
		options = { silent = true },
		description = "Clear search highlight",
	},
	--     {
	--         mode = { "n" },
	--         lhs = "aa",
	--         rhs = ":AerialToggle<CR>",
	--         options = { silent = true },
	--         description = "",
	--     },
})
