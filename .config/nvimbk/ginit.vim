GuiRenderLigatures 1
"
" " Disable GUI Tabline
" if exists(':GuiTabline')
"     GuiTabline 0
" endif
"
" " Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 1
 endif
