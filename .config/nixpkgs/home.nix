{ config, pkgs, home-environment, ... }:

{

  home.username = "joao";
  home.homeDirectory = "/home/joao";

  home.packages = [
    pkgs.neovim
    pkgs.neovim-qt
    pkgs.tree-sitter
    # pkgs.gcc
    pkgs.ripgrep
    pkgs.fd

    # pkgs.python311
    # pkgs.python310Packages.pip

    pkgs.git
    pkgs.gitui
    pkgs.lazygit

    # pkgs.nodejs
    # pkgs.nodePackages.npm
  ];

  home.stateVersion = "22.11";

  programs.home-manager.enable = true;

  programs.neovim = {
    enable = false;
    # defaultEditor = true;
    # withPython3 = true;
  };

  targets.genericLinux.enable = true;

  # environment.variables.NIX_BUILD_SHELL = "zsh"; 
  # environment.variables.EDITOR = "neovim";

}
